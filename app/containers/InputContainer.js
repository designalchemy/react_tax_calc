var React = require('react');

var InputContainer = React.createClass({

	handleUserInput: function (e) {
		var wage = e.target.value;
		this.props.onChange(wage);
	},

	setStudentLoan: function () {
		this.props.onLoanChange();
	},

	setSelfEmployed: function () {
		this.props.onSeChange();
	},

	render: function () {
		return (
			<div>
				<div className="row">
					<span className="col-xs-6 col-sm-2" style={{paddingTop: '3px'}}>
						Your Yearly Wage
					</span>
					<span className="col-xs-6 col-sm-2">
						<input type="number" onChange={this.handleUserInput} style={{width: '90%'}} />
					</span>
					<span className="col-xs-12 col-sm-2" style={{paddingTop: '3px'}}>
						Student Loan?&nbsp;&nbsp;
						<input type="checkbox" onChange={this.setStudentLoan} />
					</span>
					<span className="col-xs-12 col-sm-2" style={{paddingTop: '3px'}}>
						Self Employed?&nbsp;&nbsp;
						<input type="checkbox" onChange={this.setSelfEmployed} />
					</span>
				</div>
				<hr />
			</div>
		);
	}

});

module.exports = InputContainer;