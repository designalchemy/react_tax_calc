var React = require('react');

var InputContainer 	= require('./InputContainer');
var OutputContainer = require('./OutputContainer');

var AppContainer = React.createClass({

	getInitialState: function () {
		return { 
			userWage: 0,
			studentLoan: false,
		};
	},

	setUserWage: function (e) {
		this.setState({
			userWage: e
		});
	},

	setLoan: function () {
		this.setState({
			studentLoan: ! this.state.studentLoan
		});
	},

	setSe: function () {
		this.setState({
			selfEmployed: ! this.state.selfEmployed
		});
	},

	render: function () {
		return (
			<main className="col-xs-12 col-lg-10 panel panel-default" style={{padding: '20px', float: 'none', margin: '0 auto'}}>
				<InputContainer onChange={this.setUserWage} onLoanChange={this.setLoan} onSeChange={this.setSe} />
				<OutputContainer userWage={this.state.userWage} studentLoan={this.state.studentLoan} selfEmployed={this.state.selfEmployed} />
			</main>
		);
	}

});

module.exports = AppContainer;