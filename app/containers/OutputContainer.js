var React = require('react');

var WageRow = require('../components/WageRow');
var WageHeader = require('../components/WageHeader');

var OutputContainer = React.createClass({

	getInitialState: function () {
		return { 
			cssId: 0,
			
		};
	},

	taxFree: function (wage) {
		var amount = 0;

		if (wage >= 122000) {
			amount = 0;
		} else if (wage > 100000) {
			amount = 11000 - ((wage - 100000) / 2 );
		} else {
			amount = 11000;
		}

		return amount;
	},

	taxDeductible: function (wage) {
		var deductible = 0;
		var taxFree = this.taxFree(wage);

		if (wage > taxFree) {
			deductible = wage - taxFree;
		} else {
			deductible = 0;
		}

		return deductible;
	},

	calcTax: function (wage) {
		var taxPaid = 0;
		var taxFreeAmmount = this.taxFree(wage);
		var taxableWage = this.taxDeductible(wage);
		var basicLimit = 43000;
		var higherLimit = 150000;
		var basicMaxTax = 6400; //static is bad but stupid tax rules

		if (wage > higherLimit) {

			var higherMaxTax = (higherLimit - basicLimit) * 0.4;
			var higherLimitTax = (wage - higherLimit) * 0.45;

			taxPaid = basicMaxTax + higherMaxTax + higherLimitTax;

		} else if (wage > basicLimit) {

			var midRange = (wage - basicLimit) * 0.4;

			taxPaid = basicMaxTax + midRange;

		} else if (taxableWage > 0) {

			taxPaid = taxableWage * 0.2;

		}

		// nasty hack for this crazy 60% tax thing
		if (wage > 100000) {

			var overkillTax = Math.min((wage - 100000), 22000);

			taxPaid = taxPaid + (overkillTax * 0.2);

		}

		return taxPaid;
	},

	calcNi: function (wage) {
		var niPaid = 0;
		var niMax = 43004;
		var niMin = 8060;
		var taxRate = this.props.selfEmployed ? 0.09 : 0.12;
		var niMinPaid = (niMax - niMin) * taxRate;

		if (wage > niMax) {

			var upperPaid = (wage - niMax) * 0.02;
			niPaid = niMinPaid + upperPaid;

		} else if (wage > niMin) {

			niPaid = (wage - niMin) * taxRate;

		}

		return niPaid;
	},

	studentLoanTax: function (wage) {
		var tax = 0;
		var loanLimit = 17495;

		if (this.props.studentLoan && wage > loanLimit) {

			tax = (wage - loanLimit) * 0.09;

		}

		return tax;
	},

	getWage: function () {
		var wage = this.props.userWage;

		if (!wage) {
			wage = 0;
		}

		return wage;
	},



	changeCss: function (e) {
		this.setState({
			cssId: e
		});
	},

	render: function () {
		var wage = this.getWage();
		var taxDue = this.calcTax(wage);
		var niDue = this.calcNi(wage);
		var studentLoan = this.studentLoanTax(wage);
		var paySeNi = this.props.selfEmployed ? 145.60 : 0;
		var deductions = taxDue + niDue + studentLoan + paySeNi;
		var cssId = this.state.cssId;

		var wageObj = {
			'Gross': wage,
			'Tax Free': this.taxFree(wage),
			'Tax Deductible': this.taxDeductible(wage),
			'Tax Due': taxDue,
			'NI Due': niDue,
			'Class 2 NI Due': paySeNi,
			'Student Loan': studentLoan,
			'Total Deductions': deductions,
			'Net Pay': wage - deductions 
		};

		var WageRowArr = [];

		Object.keys(wageObj).forEach(function (key) {
			var title = key;
			var ammount = wageObj[key];

			WageRowArr.push(<WageRow title={title} ammount={ammount} key={key} whichCssId={cssId} />);
		});

		return (
			<div>

				<WageHeader onCssChange={this.changeCss}/>

				{WageRowArr}

			</div>
		);
	}

});

module.exports = OutputContainer;