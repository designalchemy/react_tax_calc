var React = require('react');

var HeaderContainer = React.createClass({

	render: function () {
		return (
			<header>
				<h1 style={{textAlign: 'center'}}>2016/17 UK Tax Calculator</h1>
				<br />
			</header>
		);
	}

});

module.exports = HeaderContainer;