var React = require('react');
var ReactDOM = require('react-dom');

var AppContainer = require('./containers/AppContainer');
var HeaderContainer = require('./containers/HeaderContainer');
var FooterContainer = require('./containers/FooterContainer');

var App = React.createClass({

	render: function () {
		return (
			<div className="container">
				<div className="row">
					<HeaderContainer />
					<AppContainer />
					<FooterContainer />
				</div>
			</div>		
		);
	}

});

ReactDOM.render(
	<App />,
	document.getElementById('app')
);