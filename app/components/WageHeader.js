var React = require('react');

var WageHeader = React.createClass({

	changeVisible: function (e) {
		var target = e.target.value;
		this.props.onCssChange(target); 
	},

	render: function () {

	    return (

	    	<div>

		    	<div className="row hidden-xs">
		    		<span className="col-sm-2"></span>
		    		<span className="col-sm-2">Yearly</span>
		    		<span className="col-xs-2">Monthly</span>
		    		<span className="col-xs-2">Weekly</span>
		    		<span className="col-xs-2">Daily</span>
		    		<span className="col-xs-2">Hourly</span>
		    	</div>
		    	<div className="row visible-xs-block">
		    		<span className="col-xs-6">
		    			Display
	    			</span>
	    			<span className="col-xs-6">
				    	<select onChange={this.changeVisible}>
				    	  <option value="yearly">Yearly</option>
				    	  <option value="monthly">Monthly</option>
				    	  <option value="weekly">Weekly</option>
				    	  <option value="daily">Daily</option>
				    	  <option value="hourly">Hourly</option>
				    	</select>
			    	</span>
		    	</div>

		    	<hr />

	    	</div>

    	);

	}

});

module.exports = WageHeader;