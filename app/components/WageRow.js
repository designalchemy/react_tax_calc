var React = require('react');

var WageRow = React.createClass({

	fixNumber: function (x) {

		return x.toFixed(2).toLocaleString();

	},

	render: function () {

		var ammount = this.props.ammount;
		var title = this.props.title;

		var css = {
			paddingBottom: '5px',
		};

		if (title == 'Net Pay') {
			Object.assign(css, {fontSize: '20px', color: 'green', marginTop: '5px', fontWeight: '400'});
		}

		if (title == 'Total Deductions') {
			Object.assign(css, {color: 'red'});
		}

		if (title == 'Student Loan') {

			Object.assign(css, {color: 'orange'});

			if (!ammount) {
				return false;
			}
		}

		if (title == 'Class 2 NI Due' && !ammount) {
			return false;
		}

		ammount = parseFloat(ammount);

		var row = this.props.whichCssId;

	    return (

	    	<div className="row" style={css}>
	    		<span className="col-xs-6 col-sm-2" style={{color: '#333'}}>{title}</span>
	    		<span className="col-xs-6 col-sm-2">£{ this.fixNumber(ammount) }</span>
	    		<span className="col-xs-2 hidden-xs">£{ this.fixNumber(ammount / 12) }</span>
	    		<span className="col-xs-2 hidden-xs">£{ this.fixNumber(ammount / 52) }</span>
	    		<span className="col-xs-2 hidden-xs">£{ this.fixNumber(ammount / 12 / 4 / 5) }</span>
	    		<span className="col-xs-2 hidden-xs">£{ this.fixNumber(ammount / 12 / 4 / 5 / 7.5) }</span>
	    	</div>

    	);

	}

});

module.exports = WageRow;